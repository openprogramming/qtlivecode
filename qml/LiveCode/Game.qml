import QtQuick 2.0

Rectangle {
    id: game

    property int usedTiles: 0

    Grid {
        id: grid
        anchors.fill: parent
        columns: 3
        rows: 3

        Repeater {
            model: 9
            delegate: Tile {
                width: grid.width/grid.columns
                height:grid.height/grid.rows

                onUsedChanged: {
                    game.usedTiles++
                }

                onPlayerMoveDone: {
                    game.computerMove()
                }
            }
        }
    }

    function computerMove() {
        if (game.usedTiles == 9)
            return

        var number
        var child
        do {
            number = Math.round(Math.random()*8)
            child = grid.children[number]
        } while (child.used)

        child.color = "red"
        child.used = true


    }
}
