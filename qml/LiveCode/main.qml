import QtQuick 2.0

Rectangle {
    id: root
    width: 480
    height: 480

    property int size: Math.min(root.width, root.height)

    Game {
        id: game
        width: root.size
        height: root.size
    }
}
