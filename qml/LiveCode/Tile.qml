import QtQuick 2.0

Rectangle {
    id: tile
    color: "grey"
    border.color: "black"
    border.width: 2

    property bool used: false

    signal playerMoveDone

    MouseArea {
        anchors.fill: parent
        onClicked: {
            if (tile.used)
                return

            tile.color = "white"
            tile.used = true
            tile.playerMoveDone()
        }
    }

    Behavior on color { ColorAnimation { duration: 300 } }
}
